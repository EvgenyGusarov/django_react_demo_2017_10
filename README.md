# README #

A simple Django project with ReactJS on frontend.

A project imitates bank accounts management panel. One can transfer money from one account to another.
If destination INN is shared between many accounts, then transfer sum is divided by number of receivers.

This project is intended to demonstrate some Django and ReactJS skills.
