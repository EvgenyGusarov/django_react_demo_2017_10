import path from 'path'
import 'webpack'

export default {
   entry:  './web_client/app.js',
   output:  {
       path: `${__dirname}/mysite/static`,
       filename: 'app.js'
   },
   resolve: {
       extensions: ['.js', '.jsx', '.scss', '.sass', '.css']
   },
   module: {
       loaders: [
           {
               test: /\.jsx?$/,
               loader: ['babel-loader?plugins[]=transform-runtime,presets[]=env,presets[]=stage-0,presets[]=react'],
               include: [
                   path.resolve(__dirname, "web_client")
               ],
           },
           {
               test: /\.(sass|scss|css)$/,
               use: [
                   {loader: "style-loader"},
                   {loader: "css-loader"},
                   {
                       loader: 'sass-loader',
                       options: {
                           includePaths: [
                               path.resolve(__dirname, "./web_client"),
                               path.resolve(__dirname, "./node_modules/bootstrap-sass/assets/stylesheets/bootstrap/")
                           ]
                       }
                   },
               ]
           },
           {
               test: /\.(png|woff|woff2|eot|ttf|svg)$/,
               loader: 'url-loader?limit=100000'
           }
       ]
   }
}