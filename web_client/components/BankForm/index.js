import React from 'react'
import { Field, reduxForm, SubmissionError } from 'redux-form'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Select2 from 'react-select2-wrapper'

import 'react-select2-wrapper/css/select2.css';
import 'bootstrap/dist/css/bootstrap.css'
import './style.sass'


const renderField = ({ input, label, type, meta: { touched, error } }) =>
  <div className={`form-group ${touched && error && "has-error"}`}>
    <label>
      {label}
    </label>
    <div>
      <input {...input} placeholder={label} type={type} className="form-control" />
      {touched &&
        error &&
        <span className="text-danger">
          {error}
        </span>}
    </div>
  </div>


const render_sender = ({ input, label, type, meta: { touched, error } }) => (
  <div className="form-group">
    <label>{label}</label>
    <Select2 style={{width: '100%'}} {...input}
     options={
       {
         placeholder: 'введите ИНН или username',
         ajax: {
           url: '/api/user/',
           dataType: 'json',
           data: function (params) {
             // Query parameters will be ?search=[term]
             return {
               search: params.term,
             };
           },
           processResults: function (data) {
             data.results.forEach(user => user.text = `${user.username}, ИНН ${user.inn || ''}`);
             return {
               results: data.results
             };
           }
         }
       }
     }
    />
    {touched &&
      error &&
      <span className="text-danger">
        {error}
      </span>}
  </div>
);


const required = value => value ? undefined : 'Обязательное поле';


class BankForm extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  checkStatus(response) {
    return response.json().then(data => {
      if (response.status >= 200 && response.status < 300) {
        console.log('submit success');
      } else {
        console.log({...data, _error: data.non_field_errors});
        throw new SubmissionError({...data, _error: data.non_field_errors});
      }
    });
  }

  submit(values) {
    console.log('submit');
    const url = '/api/transfer/';
    const body = JSON.stringify(values);

    return fetch(url, {
      method: 'POST',
      headers: {
        "Content-type": "application/json; charset=UTF-8",
        "Accept-Language": "ru"
      },
      body: body
    }).
    then(response => this.checkStatus(response));
  }

  render() {
    const { handleSubmit, pristine, reset, submitting, submitSucceeded, submitFailed, error } = this.props;
    return (
      <form onSubmit={ handleSubmit(this.submit) }>

        { submitSucceeded && <div className="alert-success">Перевод совершен успешно</div>}
        { submitFailed && <div className="alert alert-danger"> {error ? `Ошибка: ${error}` : 'Ошибка'} </div>}

        <Field name="sender" component={ render_sender } label="Отправитель" validate={[required]}/>

        <Field name="receiver" type="text" component={renderField} label="Получатель" validate={[required]} />

        <Field name="amount" type="text" component={renderField} label="Сумма перевода" validate={[required]} />

        <button type="submit" className="btn btn-primary" disabled={pristine || submitting}>
          Submit
        </button>

      </form>
    )
  }
}


export default reduxForm({form: 'bank_form'})(BankForm);
