import React from 'react'
import BankForm from '../BankForm'


class BankPage extends React.Component {
  render() {
    return (
        <div className="row">
            <div className="col-md-3">
                <BankForm />
            </div>
        </div>
    )
  }
}


export default BankPage;
