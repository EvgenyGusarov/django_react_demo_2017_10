import React, { Component } from 'react'
import {
    BrowserRouter,
    Route,
    Link,
    Switch,
    Redirect
} from 'react-router-dom'
import { Provider } from 'react-redux'
import PropTypes from 'prop-types'
import Bank from '../Bank'

// import 'bootstrap-sass'


const App = ({ store }) => (
    <Provider store={store}>
        <BrowserRouter>
            <div className="container">
                <Switch>
                    <Route path="/" component={Bank} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>
);

App.propTypes = {
  store: PropTypes.object.isRequired
};

export default App