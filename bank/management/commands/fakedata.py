import random
import math
from django.core.management.base import BaseCommand, CommandError
from django_seed import Seed

from bank.models import MyUser


seeder = Seed.seeder()


def random_inn(size):
    return ''.join(map(str, [random.randint(0, 9) for i in range(size)]))


class Command(BaseCommand):
    help = 'Bulk create fake users'

    def add_arguments(self, parser):
        parser.add_argument('users_count', nargs=1, type=int)

    def handle(self, *args, **options):
        users_count = options['users_count'][0]

        # Pool size is less than users_count to create some duplicates
        inn_pool_size = math.floor(users_count * 0.8)
        if inn_pool_size == 0:
            raise CommandError('users_count too small')

        inn_pool = [random_inn(12) for i in range(inn_pool_size)]

        if users_count > 0:
            seeder.add_entity(MyUser, users_count, {
                'inn': lambda x: random.choice(inn_pool),
                'account': lambda x: random.randint(0, 100) * 100
            })
        else:
            raise CommandError('wrong users_count')

        seeder.execute()
