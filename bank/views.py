from django.db import transaction
from rest_framework.views import APIView
from rest_framework.generics import ListAPIView
from rest_framework import filters
from rest_framework.response import Response

from .models import MyUser
from .serializers import MyUserSerializer, TransferSerializer


class MyUserListAPIView(ListAPIView):
    serializer_class = MyUserSerializer
    queryset = MyUser.objects.all().order_by('-id')
    filter_backends = (filters.SearchFilter, )
    search_fields = ('^inn', '^username', )


class TransferAPIView(APIView):
    def post(self, request):
        serializer = TransferSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        sender = serializer.validated_data['sender']
        amount = serializer.validated_data['amount']
        receivers = serializer.validated_data['receivers']

        share = amount / len(receivers)

        with transaction.atomic():
            sender.account -= amount
            sender.save()
            for receiver in receivers:
                receiver.account += share
                receiver.save()

        return Response({})
