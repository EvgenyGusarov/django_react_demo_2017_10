from django.db import models
from django.contrib.auth.models import AbstractBaseUser, AbstractUser
from django.core.validators import RegexValidator


INN_REGEX = r'\d{10,12}'
INN_ERROR_MESSAGE = 'ИНН должен состоять из 10 или 12 цифр'


class MyUser(AbstractUser):
    inn = models.CharField(
        max_length=12, validators=[RegexValidator(INN_REGEX, message=INN_ERROR_MESSAGE)],
        blank=True, null=True, db_index=True)
    account = models.DecimalField(decimal_places=2, max_digits=18, blank=True, default=0)
