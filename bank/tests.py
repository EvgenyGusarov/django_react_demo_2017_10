from decimal import Decimal
from rest_framework import status
from rest_framework.test import APITestCase
from .models import MyUser


class SearchTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.users = [
            MyUser.objects.create(username='user1', inn='111222333000', account=100),
            MyUser.objects.create(username='user2', inn='111222333000', account=100),
            MyUser.objects.create(username='user3', inn='111222333001', account=100),
            MyUser.objects.create(username='user4', inn='111222333011', account=100),
        ]

    def test_empty(self):
        response = self.client.get('/api/user/', data={})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), len(self.users))

    def test_not_found(self):
        response = self.client.get('/api/user/', data=dict(search=88))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 0)

    def test_unique_inn(self):
        response = self.client.get('/api/user/', data=dict(search=111222333001))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['username'], 'user3')

        response = self.client.get('/api/user/', data=dict(search=11122233301))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(response.data['results'][0]['username'], 'user4')

    def test_duplicate_inn(self):
        response = self.client.get('/api/user/', data=dict(search=11122233300))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 3)

        response = self.client.get('/api/user/', data=dict(search=111222333000))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)


class TransferTestCase(APITestCase):
    @classmethod
    def setUpTestData(cls):
        cls.users = [
            MyUser.objects.create(id=1, username='user1', inn='111222333000', account=100),
            MyUser.objects.create(id=2, username='user2', inn='111222333000', account=200),
            MyUser.objects.create(id=3, username='user3', inn='111222333001', account=300),
            MyUser.objects.create(id=4, username='user4', inn='111222333011', account=400),
        ]

    def check_account(self, user_id, value):
        self.assertEqual(MyUser.objects.get(id=user_id).account, value)

    def test_one_to_one(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=10))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_account(3, Decimal('290.00'))
        self.check_account(4, Decimal('410.00'))

    def test_fraction(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=0.23))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_account(3, Decimal('299.77'))
        self.check_account(4, Decimal('400.23'))

    def test_cent(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=0.01))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_account(3, Decimal('299.99'))
        self.check_account(4, Decimal('400.01'))

    def test_one_to_many(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333000', amount=0.24))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_account(3, Decimal('299.76'))
        self.check_account(1, Decimal('100.12'))
        self.check_account(2, Decimal('200.12'))

    def test_transfer_all(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=300))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.check_account(3, 0)
        self.check_account(4, 700)

    def test_amount_too_large(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=300.01))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.check_account(3, 300)
        self.check_account(4, 400)

    def test_invalid_sender(self):
        response = self.client.post('/api/transfer/', data=dict(receiver='111222333011', amount=100))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=33, receiver='111222333011', amount=100))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender='asdf', receiver='111222333011', amount=100))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.check_account(4, 400)

    def test_invalid_receiver(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, amount=100))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333555', amount=100))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.check_account(3, 300)

    def test_invalid_amount(self):
        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=100.001))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount='asdf'))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=0))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        response = self.client.post('/api/transfer/', data=dict(sender=3, receiver='111222333011', amount=-0.1))
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        self.check_account(3, 300)
        self.check_account(4, 400)
