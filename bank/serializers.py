from decimal import Decimal
from rest_framework import serializers
from .models import MyUser, INN_REGEX, INN_ERROR_MESSAGE


class MyUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = MyUser
        fields = ('id', 'username', 'inn', )


class TransferSerializer(serializers.Serializer):
    sender = serializers.PrimaryKeyRelatedField(queryset=MyUser.objects.all())
    receiver = serializers.RegexField(INN_REGEX, error_messages={'invalid': INN_ERROR_MESSAGE})
    amount = serializers.DecimalField(max_digits=18, decimal_places=2, min_value=Decimal('0.01'))

    def validate_receiver(self, value):
        if not MyUser.objects.filter(inn=value).exists():
            raise serializers.ValidationError('Пользователей с указанным ИНН не найдено')
        return value

    def validate(self, attrs):
        if attrs['amount'] > attrs['sender'].account:
            raise serializers.ValidationError('Недостаточно средств на счете')

        inn = attrs['receiver']
        receivers = list(MyUser.objects.filter(inn=inn))

        attrs['receivers'] = receivers

        return attrs
